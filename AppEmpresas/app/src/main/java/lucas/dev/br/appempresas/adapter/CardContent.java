package lucas.dev.br.appempresas.adapter;



/**
 * Created by lucas on 23/04/17.
 */

public class CardContent
{
    private String company;
    private String business;
    private String location;
    private String image;

    public CardContent( String company, String bussines, String location, String image )
    {
        this.company = company;
        this.business = bussines;
        this.location = location;
        this.image = image;
    }


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }
}
