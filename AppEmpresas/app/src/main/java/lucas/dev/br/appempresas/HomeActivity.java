package lucas.dev.br.appempresas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lucas.dev.br.appempresas.adapter.CardContent;
import lucas.dev.br.appempresas.adapter.RecyclerItemClickListener;
import lucas.dev.br.appempresas.adapter.RecyclerViewAdapter;
import lucas.dev.br.appempresas.models.Enterprises;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class HomeActivity extends AppCompatActivity {

    private String access_token;

    private String client;

    private String uid;

    private TextView userName;

    private String investorName;

    private Retrofit retrofit = MainActivity.retrofit;

    private RelativeLayout relativeLayout;

    private ProgressBar progressBar;

    private RecyclerView mRecyclerView;

    private RecyclerViewAdapter recyclerViewAdapter;

    LinearLayoutManager linearLayoutManager;

    private List<CardContent> mList;

    private List<String> mDescriptionList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        mRecyclerView = ( RecyclerView )findViewById(R.id.recyclerListView);

        userName = (TextView)findViewById(R.id.userName);

        relativeLayout = (RelativeLayout)findViewById(R.id.relativeLayout);

        progressBar = (ProgressBar)findViewById(R.id.progressBar_home);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        getHeaders( bundle );

        userName.setText(getTime() + " " + investorName + " !");

        initializeData();

        mRecyclerView = ( RecyclerView )findViewById(R.id.recyclerListView);

        mRecyclerView.setHasFixedSize( true );

        linearLayoutManager = new LinearLayoutManager( this );

        mRecyclerView.addOnItemTouchListener( new RecyclerItemClickListener(getApplicationContext(),
                new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = new Intent(HomeActivity.this, EnterpriseInfo.class);

                intent.putExtra("enterprise_name", mList.get(position).getCompany());

                intent.putExtra("description", mDescriptionList.get(position));

                startActivity(intent);

                overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);

            }
        }) );

        Toast.makeText(getApplicationContext(), "A busca pode ser feita pelo nome da empresa ou pelo tipo da empresa ou pela cidade em que se localiza", Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);

        SearchView searchView = (SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                query = query.toLowerCase();

                final  List<CardContent> filterList = new ArrayList<CardContent>();

                for ( int i = 0; i < mList.size(); i++) {

                    final String companyName = mList.get(i).getCompany().toLowerCase();

                    final String companyCity = mList.get(i).getLocation().toLowerCase();

                    final String companyType = mList.get(i).getBusiness().toLowerCase();

                    if(companyName.contains(query)) {

                        filterList.add(mList.get(i));

                    }else if(companyCity.contains(query)){

                        filterList.add(mList.get(i));

                    }else if(companyType.contains(query)){

                        filterList.add(mList.get(i));
                    }
                }

                mRecyclerView.setLayoutManager( linearLayoutManager );

                recyclerViewAdapter = new RecyclerViewAdapter(filterList);

                mRecyclerView.setAdapter(recyclerViewAdapter);

                recyclerViewAdapter.notifyDataSetChanged();

                return true;
            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    private void getHeaders( Bundle bundle ){

        if( bundle != null ){

            access_token = (String)bundle.get("access_token");
            client = (String)bundle.get("client");
            uid = (String)bundle.get("uid");
            investorName = (String)bundle.get("userName");
            Log.d(HomeActivity.class.getSimpleName(), access_token + " " + client + " " + uid);

        }else {

            Log.d(HomeActivity.class.getSimpleName(), "bundle = null");
        }
    }


    private void initializeData()
    {
        mList = new ArrayList<>();

        retrofit.create(Repositorio.class).showAll(access_token,client,uid).enqueue(new Callback<Enterprises>() {
            @Override
            public void onResponse(Response<Enterprises> response, Retrofit retrofit) {

                progressBar.setIndeterminate(true);

                Log.d(HomeActivity.class.getSimpleName(), "onResponse");

                Log.d(HomeActivity.class.getSimpleName(), response.message());

                if (response.isSuccess())
                {

                    relativeLayout.setVisibility(View.GONE);

                    mRecyclerView.setVisibility(View.VISIBLE);

                    mList = new ArrayList<CardContent>();

                    mDescriptionList = new ArrayList<String>();

                    for(int i = 0; i < response.body().getListEnterprise().size(); i++){

                        mList.add(new CardContent(response.body().getListEnterprise().get(i).getEnterprise_name()
                                ,response.body().getListEnterprise().get(i).getEnterprise_type().getEnterprise_type_name()
                                ,response.body().getListEnterprise().get(i).getCity()
                                ,response.body().getListEnterprise().get(i).getEnterprise_name().substring(0,2) ));

                        mDescriptionList.add(response.body().getListEnterprise().get(i).getDescription());

                    }

                    mRecyclerView.setLayoutManager( linearLayoutManager );

                    recyclerViewAdapter = new RecyclerViewAdapter( mList );

                    mRecyclerView.setAdapter( recyclerViewAdapter );

                    recyclerViewAdapter = new RecyclerViewAdapter( mList );

                    mRecyclerView.setAdapter( recyclerViewAdapter );

                    recyclerViewAdapter.notifyDataSetChanged();


                    Log.d(HomeActivity.class.getSimpleName(), ""+response.body().getListEnterprise().size());

                    Log.d(HomeActivity.class.getSimpleName(), "isSuccess");
                }
            }

            @Override
            public void onFailure(Throwable t) {

                relativeLayout.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });
    }

    private String getTime() {

        Calendar calendar = Calendar.getInstance();

        double time = calendar.get(Calendar.HOUR_OF_DAY);

        String currentTime = "";

        Log.i("Time: ", "" + time);

        if (time >= 5 && time <= 11) {

            currentTime = "Bom Dia ";

        } else if (time > 11 && time < 18) {

            currentTime = "Boa Tarde ";

        } else {

            currentTime = "Boa Noite ";
        }

        return currentTime;
    }
}
