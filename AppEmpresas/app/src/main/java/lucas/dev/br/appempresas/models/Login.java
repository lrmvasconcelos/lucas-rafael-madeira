package lucas.dev.br.appempresas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/07/17.
 */

public class Login
{
    @SerializedName("investor")
    @Expose
    private Investidor investidor;

    @SerializedName("enterprise")
    @Expose
    private String empresa;

    @SerializedName("success")
    @Expose
    private Boolean sucesso;

    public Investidor getInvestidor() {
        return investidor;
    }

    public void setInvestidor(Investidor investidor) {
        this.investidor = investidor;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Boolean getSucesso() {
        return sucesso;
    }

    public void setSucesso(Boolean sucesso) {
        this.sucesso = sucesso;
    }
}
