package lucas.dev.br.appempresas.adapter;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import java.util.List;

import lucas.dev.br.appempresas.R;

/**
 * Created by lucas on 23/04/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.LocalViewHolder> {

    private List<CardContent> mList;

    private int FADE_DURATION = 500;


    public RecyclerViewAdapter(List<CardContent> mList){
        this.mList = mList;
    }

    @Override
    public LocalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate(R.layout.card_view, parent, false);
        LocalViewHolder localViewHolder = new LocalViewHolder( view );
        return localViewHolder;    }


    @Override
    public void onBindViewHolder(LocalViewHolder holder, int position) {

        holder.companyName.setText(mList.get(position).getCompany());

        holder.business.setText(mList.get(position).getBusiness());

        holder.location.setText(mList.get(position).getLocation());

        holder.companyTextTitle.setText(mList.get(position).getImage());

        setScaleAnimation(holder.itemView);


    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class LocalViewHolder extends  RecyclerView.ViewHolder {

        CardView cardView;

        TextView companyName, business, location, companyTextTitle;



        LocalViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView)itemView.findViewById( R.id.cardView );

            companyName = (TextView)itemView.findViewById( R.id.companyName );

            business = (TextView)itemView.findViewById( R.id.business );

            location = (TextView)itemView.findViewById( R.id.location );

            companyTextTitle = (TextView) itemView.findViewById( R.id.companyTextTitle );

        }

    }


    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

}
