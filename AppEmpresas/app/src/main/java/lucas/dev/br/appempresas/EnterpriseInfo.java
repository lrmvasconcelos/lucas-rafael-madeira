package lucas.dev.br.appempresas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import lucas.dev.br.appempresas.models.Enterprise;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EnterpriseInfo extends AppCompatActivity {

    private String description;

    private TextView companyTextTitle_enterprise, companyDescription_enterprise;

    private String companyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_info);

        companyTextTitle_enterprise = (TextView)findViewById(R.id.companyTextTitle_enterprise);

        companyDescription_enterprise = (TextView)findViewById(R.id.companyDescription_enterprise);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        getHeaders( bundle );

    }

    private void getHeaders( Bundle bundle ){

        if( bundle != null ){

            description = (String)bundle.get("description");
            companyName = (String)bundle.get("enterprise_name");

            companyTextTitle_enterprise.setText(companyName.substring(0,2));
            companyDescription_enterprise.setText(description);

        }else {

            Log.d(HomeActivity.class.getSimpleName(), "bundle = null");
        }
    }
}
