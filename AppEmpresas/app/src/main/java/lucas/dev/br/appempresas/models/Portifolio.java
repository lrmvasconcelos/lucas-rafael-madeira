package lucas.dev.br.appempresas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/07/17.
 */

public class Portifolio {

    @SerializedName("enterprises_number")
    @Expose
    private Integer enterprises_number;


    @SerializedName("enterprises")
    @Expose
    private Enterprises enterprises;
}
