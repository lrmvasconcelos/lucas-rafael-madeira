package lucas.dev.br.appempresas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/07/17.
 */

public class Investidor {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("investor_name")
    @Expose
    private String investor_name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("balance")
    @Expose
    private Integer balance;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("portfolio_value")
    @Expose
    private Integer portfolio_value;

    @SerializedName("first_access")
    @Expose
    private Boolean first_access;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getPortfolio_value() {
        return portfolio_value;
    }

    public void setPortfolio_value(Integer portfolio_value) {
        this.portfolio_value = portfolio_value;
    }

    public Boolean getFirst_access() {
        return first_access;
    }

    public void setFirst_access(Boolean first_access) {
        this.first_access = first_access;
    }

    public Boolean getSuper_angel() {
        return super_angel;
    }

    public void setSuper_angel(Boolean super_angel) {
        this.super_angel = super_angel;
    }

    @SerializedName("super_angel")
    @Expose
    private Boolean super_angel;






}
