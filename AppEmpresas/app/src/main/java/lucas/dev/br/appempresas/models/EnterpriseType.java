package lucas.dev.br.appempresas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/07/17.
 */

public class EnterpriseType {


    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("enterprise_type_name")
    @Expose
    private String enterprise_type_name;

    public Integer getEmail_enterprise() {
        return id;
    }

    public void setEmail_enterprise(Integer email_enterprise) {
        this.id = email_enterprise;
    }

    public String getEnterprise_type_name() {
        return enterprise_type_name;
    }

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }

}
