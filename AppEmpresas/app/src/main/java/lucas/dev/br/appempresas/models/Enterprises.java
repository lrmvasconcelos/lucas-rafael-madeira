package lucas.dev.br.appempresas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lucas on 06/07/17.
 */

public class Enterprises {

    public List<Enterprise> getListEnterprise() {
        return listEnterprise;
    }

    public void setListEnterprise(List<Enterprise> listEnterprise) {
        this.listEnterprise = listEnterprise;
    }

    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> listEnterprise;


}
