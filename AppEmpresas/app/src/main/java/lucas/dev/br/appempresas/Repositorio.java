package lucas.dev.br.appempresas;

import java.util.HashMap;

import lucas.dev.br.appempresas.models.Enterprise;
import lucas.dev.br.appempresas.models.Enterprises;
import lucas.dev.br.appempresas.models.Login;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by lucas on 06/07/17.
 */

public interface Repositorio {

    @POST("users/auth/sign_in/")
    Call<Login> login(@Body HashMap<String, String> body);

    @GET("enterprises")
    Call<Enterprises> showAll(@Header("access-token") String token, @Header("client")String client, @Header("uid") String uid);

    @GET("enterprises{name}")
    Call<Enterprise> getEnterpriseInfo(@Path("name") String name, @Header("access-token") String token, @Header("client")String client, @Header("uid") String uid);

}
