package lucas.dev.br.appempresas;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.HashMap;

import lucas.dev.br.appempresas.models.Login;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher{


    private EditText editText_email, editText_password;
    private ProgressBar progressBar;

    public static final  Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://54.94.179.135:8090/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
            .build();

    private Button buttonLogin;

    public CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editText_email = (EditText)findViewById( R.id.editText_email );
        editText_email.addTextChangedListener( this );



        editText_password = (EditText)findViewById( R.id.editText_password );
        editText_password.addTextChangedListener( this );

        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        buttonLogin = (Button)findViewById( R.id.buttonLogin );
        buttonLogin.setOnClickListener( this );

        Toast.makeText(getApplicationContext(), "Credenciais para acesso: testeapple@ioasys.com.br : 12341234", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.buttonLogin)
        {
            if(validateFields()) {

                progressBar.setVisibility(View.VISIBLE);

                progressBar.setIndeterminate(true);

                Log.d(MainActivity.class.getSimpleName(), "validateFields");

                HashMap<String, String> hashMap = new HashMap<>();

                hashMap.put("email", editText_email.getText().toString());

                hashMap.put("password", editText_password.getText().toString());

                retrofit.create(Repositorio.class).login(hashMap).enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Response<Login> response, Retrofit retrofit) {

                        Log.d(MainActivity.class.getSimpleName(), response.message());

                        progressBar.setVisibility(View.GONE);

                        if (response.isSuccess()) {

                            Log.d(MainActivity.class.getSimpleName(), "isSuccess");

                            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                            intent.putExtra("access_token", response.headers().get("access-token"));
                            intent.putExtra("client", response.headers().get("client"));
                            intent.putExtra("uid", response.headers().get("uid"));
                            intent.putExtra("userName", response.body().getInvestidor().getInvestor_name());
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);


                        } else {

                            Snackbar.make(coordinatorLayout, response.message(), Snackbar.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                        progressBar.setVisibility(View.GONE);

                        Snackbar.make(coordinatorLayout, t.getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void afterTextChanged(Editable editable) {
        callClearErrors(editable);
    }

    public boolean validateFields(){

        String email = editText_email.getText().toString();

        String password = editText_password.getText().toString();

        return !(isEmptyFields(email, password));
    }

    private boolean isEmptyFields(String user, String pass) {
        if (TextUtils.isEmpty(user)) {

            editText_email.requestFocus();

            editText_email.setError("Empty field");

            return true;
        } else if (TextUtils.isEmpty(pass)) {

            editText_password.requestFocus();

            editText_password.setError("Empty field");

            return true;
        }
        return false;
    }

    private void callClearErrors(Editable s) {
        if (!s.toString().isEmpty()) {

            clearErrorFields(editText_email);

            clearErrorFields(editText_password);
        }
    }

    private void clearErrorFields(EditText... editTexts) {

        for (EditText editText : editTexts) {

            editText.setError(null);
        }
    }
}
