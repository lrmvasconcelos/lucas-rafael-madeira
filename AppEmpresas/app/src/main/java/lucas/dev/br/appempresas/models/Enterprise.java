package lucas.dev.br.appempresas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/07/17.
 */

public class Enterprise {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("email_enterprise")
    @Expose
    private String email_enterprise;

    @SerializedName("facebook")
    @Expose
    private String facebook;

    @SerializedName("twitter")
    @Expose
    private String twitter;

    @SerializedName("linkedin")
    @Expose
    private String linkedin;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("own_enterprise")
    @Expose
    private String own_enterprise;

    @SerializedName("enterprise_name")
    @Expose
    private String enterprise_name;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("value")
    @Expose
    private Integer value;

    @SerializedName("share_price")
    @Expose
    private Integer share_price;

    @SerializedName("enterprise_type")
    @Expose
    private EnterpriseType enterprise_type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail_enterprise() {
        return email_enterprise;
    }

    public void setEmail_enterprise(String email_enterprise) {
        this.email_enterprise = email_enterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(String own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getShare_price() {
        return share_price;
    }

    public void setShare_price(Integer share_price) {
        this.share_price = share_price;
    }

    public EnterpriseType getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(EnterpriseType enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
